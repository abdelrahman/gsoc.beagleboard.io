.. _gsoc-2010-projects:

:far:`calendar-days` 2010
##########################

Pulse Width Modulation (PWM)
****************************

.. youtube:: d_vYMTuSGrM
   :width: 100%

| **Summary:** The main aim of this project will be to provide a high level interface for the Pulse Width Modulation output pins of the OMAP3 found on the BeagleBoard. Another aim will be to push this driver upstream(the Linux-OMAP tree). Two applications will be developed to demonstrate the use of this driver:

- Motor control using PWM signals generated by the BeagleBoard using the PWM driver.
- Writing a glue layer for ALSA to communicate with the PWM driver and output sound from the PWM output pins.

**Contributor:** Varun Jewalikar

**Mentors:** Søren Steen Christensen, Cristina Murillo

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/varun_jewalikar.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/Pulse_Width_Modulation
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

OpenCV DSP Acceleration
*************************

.. youtube:: TSgAzdaJ1Xo
   :width: 100%

| **Summary:** Use of optimized computer vision algorithm is vital to meet real-time requirements especially on popular mobile platform like BeagleBoard. OpenCV is one of the widely used computer vision library today. When designing application based on OpenCV on platform like BeagleBoard, it is desired that it could give real time performance. One way to achive this is by using on-chip DSP to accelerate OpenCV algorithms. This could also eventually boost the use of open-embedded platform like BeagleBoard. 

**Contributor:** Pramod Poudel

**Mentors:** David Anders, Andrew Bradford, Matt Porter, Luis Gustavo Lira

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/ppoudel.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/OpenCV#Documentation
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

XBMC Rendering Optimizations
*****************************

.. youtube:: gvJ32T-W3Gw
  :width: 100%

| **Summary:** XBMC, an award winning media center software, have recently been ported to ARM by mcgeagh, however its still very unusable due to slow rendering of the GUI. I would like to get a chance and really refactor parts of the GUI framework and optimize and alter alot of the rendering done in XBMC core to lighten the GPU / CPU load. If succeded this would make XBMC useable on slower hardware (beagleboard).

**Contributor:** Tobias Arrskog

**Mentors:** Mike Zucchi, Mans Rullgard, Søren Steen Christensen

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/topfs2_tobias_arrskog.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/XBMC#Documentation
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

RPC layer and POSIX wrappers for C6Run
*******************************************

.. vimeo:: 12796429
   :width: 100%

| **Summary:** The OMAP3530 DSP core is quite powerful but not easy to prototype algorithms on, since there is no standard I/O functionality available directly. The project will allow developers to use familiar POSIX functionality such as terminal output/input and file access and compile their code with ease, enabling them to focus on building and optimizing their DSP algorithm instead of dealing with the details of interprocessor communications. 

**Contributor:** Yaman Umuroglu

**Mentors:** Katie Roberts-Hoffman, Laine Walker-Avina, Frank Walzer

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/maltanar.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/C6Run#Documentation
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

USB Sniffer
*************

| **Summary:** The goal of this project is to use the BeagleBoard as an USB sniffer. The host computer would be connected to the slave USB port of the BeagleBoard, and the device to be sniffed on the host USB port. The BeagleBoard would then forward USB data, while logging it. This presents the following advantages over a software-based solution: 

- No software modification is required; 
- support of proprietary OSes; 
- allows debugging of new USB stacks; 
- possibly lower-level debugging of USB frames.

**Contributor:** Nicolas Boichat

**Mentors:** Hunyue Yau, Laine Walker-Avina, Frans Meulenbroeks

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/drinkcat.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/USBSniffer
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

NEON Support for FFTW
**************************

| **Summary:** This work presents advancements in the FFTW library, incorporating NEON instruction set support for ARM architectures within an extended SIMD interface. Further enhancements include the integration of a performance counter into the FFTW planner, enabling precise measurement of execution times for algorithmic selection. Optimized code for accelerated Fourier transforms contributes to demonstrable speedups, illustrated through visual presentations using GNU Octave. The amalgamationof these improvements establishes a more efficient and performance-driven FFTW library, showcasing the tangible benefits of SIMDextensions, performance counters, and code optimization

**Contributor:** Christopher Friedt

**Mentors:** Mans Rullgard, Philip Balister

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2010/orgs/beagleboard/projects/chrisfriedt.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2010_Projects/FFTW#Documentation
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 

   Checkout eLinux page for GSoC 2010 projects `here <https://elinux.org/BeagleBoard/GSoC/2010_Projects>`_ for more details.
