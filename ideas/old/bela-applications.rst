.. _gsoc-idea-bela-applications:

Bela Applications
##################

Bela is a platform for ultra-low latency audio and sensor applications based on 
the BeagleBoard family of boards. It uses PRU and Xenomai at its core. https://bela.io

.. card:: 
    
    :fas:`music;pst-color-secondary` **Browser-based workbench**
    ^^^^

    Bela features a browser-based oscilloscope for visualising audio and sensor signals. This project 
    would extend the oscilloscope into a more full-featured laboratory workbench.

    - The oscilloscope itself could be extended in several ways, for example:
        - More options for scaling the plot in each axis
        - Adjustable cursors for reading off measurements in time or amplitude
        - Automatic detection of amplitude or frequency from the plot
        - More options for triggering
        - XY mode
        - Updated visual appearance

    - On top of that, other workbench tools could be added, including:
        - Waveform generator
        - Multimeter (RMS amplitude, frequency, DC offset, etc.)
        - Browser-based simulation of common analog and digital inputs (buttons, potentiometers etc.) and outputs (e.g. LEDs) This project would be built primarily in JavaScript, with some secondary work in C++ to connect to the Bela core code. We are open to the inclusion of existing open-source libraries to build any of these components.

    - **Goal:** Improve the features of the Bela oscilloscope and turn it into a full-fledged workbench for audio-rate signals. Hardware Skills: Minimal
    - **Software Skills:** Javascript, HTML, frontend
    - **Possible Mentors:** giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:**
        - https://github.com/BelaPlatform/Bela/

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`music;pst-color-secondary` **Browser-based IDE**
    ^^^^

    Bela’s browser-based IDE is served by a node.js server running home-grown modules and a HTML5 
    frontend based around jquery, ace.js and scss. The codebase started out in 2015 for internal 
    development purposes, it has since been made public and has gone through two major redesigns 
    during its 6-year lifetime. It allows the user to browse projects and examples, edit files and 
    build and run projects. Over time, the frontend/backend relationship has evolved to support a 
    number of Bela-specific features.The software landscape has changed radically since 2015 and 
    there are now several great web-based IDEs out there. The scope of this project is to evaluate 
    how a Bela-like workflow and appearance could be implemented on top of a more modern and better 
    supported browser-based IDE, such as Cloud9 (or - better - something with a more liberal license). 
    You will start by verifying that the requirements for frontend-backend communication logic can 
    be met on the new platform. Then you will write a proof of concept skin for the new platform 
    that mimics, where possible, the existing Bela one. You will need knowledge of web technologies, 
    both for frontend and backend.

    - **Goal:** Improve the core foundation and maintainability of the Bela IDE. Hardware Skills: Minimal
    - **Software Skills:** Javascript / HTML / frontend
    - **Possible Mentors:** giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** 
        - https://github.com/BelaPlatform/bela-image-builder
        - https://github.com/beagleboard/image-builder 

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`